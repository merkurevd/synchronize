import jetbrains.buildServer.configs.kotlin.v2019_2.*
import jetbrains.buildServer.configs.kotlin.v2019_2.buildSteps.script
import jetbrains.buildServer.configs.kotlin.v2019_2.vcs.GitVcsRoot

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2019.2"

project {

    vcsRoot(Syncrhonize2)
    vcsRoot(Project_1)

    buildType(Dimorinny1)
}

object Dimorinny1 : BuildType({
    name = "dimorinny1"

    vcs {
        root(DslContext.settingsRoot, "+:. => system/synchronize")
        root(Syncrhonize2, "+:. => system/synchronize/synchronize2")
    }

    steps {
        script {
            name = "ls"
            scriptContent = "tree"
        }
    }
})

object Project_1 : GitVcsRoot({
    id("Project")
    name = "project"
    url = "https://bitbucket.org/merkurevd/project"
    authMethod = password {
        userName = "didika914@gmail.com"
        password = "zxx269fd1e9df78acde775d03cbe80d301b"
    }
})

object Syncrhonize2 : GitVcsRoot({
    name = "syncrhonize2"
    url = "https://merkurevd@bitbucket.org/merkurevd/second-synchronize.git"
})
